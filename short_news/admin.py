from django.contrib import admin
from short_news.models import News, Likes
from forms import NewsAdminForm
from django.db import models
# from cms.admin.placeholderadmin import PlaceholderAdmin
# Register your models here.

class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'creation_date')
    list_filter = ('creation_date',)
    search_fields = ('title',)
    form = NewsAdminForm 

class LikesAdmin(admin.ModelAdmin):
    list_display = ('liked_news', 'ip_address', 'creation_date')

admin.site.register(News, NewsAdmin)
# admin.site.register(Likes, LikesAdmin)