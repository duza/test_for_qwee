from django import forms
from models import News

class NewsAdminForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ('title', 'creation_date', 'text')
        widgets = {
        'text' : forms.Textarea(attrs={'rows':4, 'cols':140, "class": "vLargeTextField"}),
        }