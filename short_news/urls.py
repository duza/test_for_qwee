from django.conf.urls import patterns, include, url

urlpatterns = patterns('short_news.views',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'news'),
    url(r'^page/(?P<page_number>\d+)/$', 'news'),
    url(r'^get/(?P<news_id>\d+)/$', 'get_news'),
    # url(r'^addlike/(?P<news_id>\d+)/$', 'addlike'),
    url(r'^about/$', 'about'),
)
