from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.http.response import Http404
from sekizai.context  import SekizaiContext
from models import News, Likes
from django.core.paginator import Paginator
from datetime import datetime
# Create your views here.

def news(request, page_number=1):
    news_all = News.objects.all()
    current_page = Paginator(news_all, 10)
    return render_to_response('news.html', 
        {'news': current_page.page(page_number)}, 
        context_instance=RequestContext(request))

def get_news(request, news_id=None):
    if news_id is not None:
        try:
            news = News.objects.get(pk=news_id)
            likes = Likes.objects.filter(liked_news=news).count()
            errors_like = []
            args = {'news': news, 'likes':likes, 'errors': errors_like }
            if request.POST:
                user_ip = request.META.get('REMOTE_ADDR','')
                if user_ip == '':
                    errors_like.append('Sorry, but you have wrong IP address.\
                     You can not like this news')
                elif Likes.objects.filter(liked_news=news, ip_address=user_ip):
                    errors_like.append("Sorry, but you cann't like this news, \
                        because you liked it earlier.")
                else:
                    Likes.objects.create(ip_address=user_ip, 
                        creation_date=datetime.now(), 
                        liked_news=news)
                    return redirect(request.path)
        except ObjectDoesNotExist:
            raise Http404
        except Exception as unexpected_error:
            raise unexpected_error
    return render_to_response('get_news.html',
        args, context_instance=RequestContext(request))

def about(request):
    return render_to_response('about.html',{}, 
        context_instance=RequestContext(request))