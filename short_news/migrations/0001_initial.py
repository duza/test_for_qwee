# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'News'
        db.create_table(u'short_news_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('creation_date', self.gf('django.db.models.fields.DateField')()),
            ('text', self.gf('django.db.models.fields.TextField')(max_length=140)),
        ))
        db.send_create_signal(u'short_news', ['News'])

        # Adding model 'Likes'
        db.create_table(u'short_news_likes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip_address', self.gf('django.db.models.fields.IPAddressField')(max_length=15)),
            ('creation_date', self.gf('django.db.models.fields.DateField')()),
            ('liked_news', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['short_news.News'])),
        ))
        db.send_create_signal(u'short_news', ['Likes'])


    def backwards(self, orm):
        # Deleting model 'News'
        db.delete_table(u'short_news_news')

        # Deleting model 'Likes'
        db.delete_table(u'short_news_likes')


    models = {
        u'short_news.likes': {
            'Meta': {'object_name': 'Likes'},
            'creation_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'liked_news': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['short_news.News']"})
        },
        u'short_news.news': {
            'Meta': {'object_name': 'News'},
            'creation_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '140'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['short_news']