from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField( max_length=40)
    creation_date = models.DateField()
    text = models.CharField(max_length=140)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-creation_date']
        verbose_name_plural = "news"

class Likes(models.Model):
    ip_address = models.IPAddressField()
    creation_date = models.DateField()
    liked_news = models.ForeignKey(News)

    def __unicode__(self):
        return '{0} likes ip {1}'.format(liked_news, self.ip_address)

    class Meta:
        verbose_name_plural = "likes"
